#!/home/salefinder-scraper/env_salefinder_scraper/bin/python3

import random # Shuffling all items before save
import logging
from dotenv import load_dotenv # Loading env vars

# Scraper functions
from salefinder_scraper.scrapers.ulta import ulta
# from salefinder_scraper.scrapers.hm import hm_women
# from salefinder_scraper.scrapers.urbanoutfitters import urbanOutfitters_women
from salefinder_scraper.scrapers.american_threads import american_threads
from salefinder_scraper.scrapers.lululemon import lululemon
from salefinder_scraper.scrapers.nike import nike
from salefinder_scraper.scrapers.victorasecret import victoria_secret
from salefinder_scraper.scrapers.levi import levi
from salefinder_scraper.scrapers.guess import guess
from salefinder_scraper.scrapers.choosy import choosy
from salefinder_scraper.scrapers.ralph_lauren import ralph_lauren
from salefinder_scraper.scrapers.target import target

# Helpers
from salefinder_scraper.helpers.parallel import parallel_launcher
from salefinder_scraper.helpers.mongohelpers import connect_db, get_documents, insert_documents, delete_collection

# Configure logger
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s -  %(filename)s - %(funcName)s - %(levelname)s - %(message)s'
)

# Load env variables
load_dotenv()

# Scrapes and saves one store's items
def scrape_store(name, sex, stores_func, stores_pages = 0, need_selenium = False):
    logging.info(f'Scraping & saving {sex} items from {name}')
    all_items = None

    # Launches selenium pages in parallel, updates db as well
    if need_selenium:
        all_items = parallel_launcher(name, sex, stores_func, stores_pages)
    else:
        if stores_pages:
            all_items = []
            
            # Loop through each page, get items, flatten into all items
            for page in range(1, stores_pages):
                page_items = stores_func(sex, page)
                for item in page_items:
                    all_items.append(item)
        else:
            # Run stores scraper function
            all_items = stores_func(sex)

    return all_items

# Consolidates all items into one collection
def consolidate_items(db_connection):
    logging.info('Consolidating all items')
    logging.info('This may take a while...')

    # Grab all store names we have
    all_stores = get_documents('stores', db_connection)

    # Holds all items
    all_items = []

    # Get items for each store, add to master list
    for store in all_stores:
        store_name = store['name']
        items = get_documents(store_name, db_connection)

        # Add each item to all items
        for item in items:
            # Do not add item if any value is missing or 'None'
            hasError = False
            reason = None
            for key in item:
                if item[key] == 'None' or item[key] == '':
                    hasError = True
                    reason = key
                else:
                    pass

            if not hasError:
                all_items.append(item)
            else:
                logging.warn(f'Not added: {item["brand"]} - {item["name"]} - missing {reason}')
                pass
    
    # Shuffle, creates variation on display
    logging.info('Shuffling all items')
    random.shuffle(all_items)

    # Delete old items
    delete_collection('all_Items', db_connection)

    # Insert items into database
    insert_documents('all_Items', all_items, db_connection)

    logging.info('Done consolidating all items')
    return




# RUNNERS FOR CRON



# These stores are production ready
runner_stores = {
    'ulta': {
        'sex': ['female'],
        'runner_func': ulta,
        'need_selenium': False,
        'pages': None, # beautifulSoup
    },
    'americanThreads': {
        'sex': ['female'],
        'runner_func': american_threads,
        'need_selenium': True,
        'pages': 12 # Selenium
    },
    'lululemon': {
        'sex': ['male', 'female'],
        'runner_func': lululemon,
        'need_selenium': False,
        'pages': None, # api
    },
    'nike': {
        'sex': ['male', 'female'],
        'runner_func': nike,
        'need_selenium': False,
        'pages': None, # api
    },
    'victoriaSecret': {
        'sex': ['female'],
        'runner_func': victoria_secret,
        'need_selenium': False,
        'pages': None, # api
    },
    'levi': {
        'sex': ['male', 'female'],
        'runner_func': levi,
        'need_selenium': True,
        'pages': 10, # selenium
    },
    'guess': {
        'sex': ['male', 'female'],
        'runner_func': guess,
        'need_selenium': False,
        'pages': None, # beautifulsoup
    },
    'choosy': {
        'sex': ['female'],
        'runner_func': choosy,
        'need_selenium': False,
        'pages': 15, # beautifulsoup
    },
    'ralphLauren': {
        'sex': ['male', 'female'],
        'runner_func': ralph_lauren,
        'need_selenium': False,
        'pages': None,
    },
    'target': {
        'sex': [None],
        'runner_func': target,
        'need_selenium': False,
        'pages': None, # api
    }
}

# Runs job for single store
def runner_update_one(store):
    logging.info(f'Starting update for {store}')

    try:
        logging.info('Creating db connection...')
        mongo_client, db_connection = connect_db()

        # Store options
        store_func = runner_stores[store]['runner_func']
        need_selenium = runner_stores[store]['need_selenium']
        pages = runner_stores[store]['pages']
        all_store_items = []

        # Run script for sexes included
        for sex in runner_stores[store]['sex']:
            # Run the stores scraper
            all_store_items += scrape_store(store, sex, store_func, stores_pages=pages, need_selenium=need_selenium)

        logging.info(f'Total items found: {len(all_store_items)}')

        # Delete old items
        delete_collection(store, db_connection)

        # Insert items into database
        insert_documents(store, all_store_items, db_connection)

        # Updates all products records
        consolidate_items(db_connection)
    except Exception as e:
        logging.error(e)
        raise e
    finally:
        if mongo_client:
            logging.info('Closing db connection...')
            mongo_client.close()
            logging.info('Connection closed!')

# Runs jobs for all stores and consolidates
def runner_update_all():
    logging.info('Starting update for all stores')

    try:
        logging.info('Creating db connection...')
        mongo_client, db_connection = connect_db()

        # Runs all individual store scraper jobs
        for store in runner_stores:
            store_func = runner_stores[store]['runner_func']
            need_selenium = runner_stores[store]['need_selenium']
            pages = runner_stores[store]['pages']
            all_store_items = []

            # Run script for sexes included
            for sex in runner_stores[store]['sex']:
                # Run the stores scraper
                all_store_items += scrape_store(store, sex, store_func, stores_pages=pages, need_selenium=need_selenium)

            # Delete old items
            delete_collection(store, db_connection)

            # Insert items into database
            insert_documents(store, all_store_items, db_connection)

        # Updates all products records
        consolidate_items(db_connection)
    except Exception as e:
        logging.error(e)
    finally:
        if mongo_client:
            logging.info('Closing db connection...')
            mongo_client.close()
            logging.info('Connection closed!')

if __name__ == "__main__":
    # Updates all stores products and updates all_Items
    runner_update_all()
