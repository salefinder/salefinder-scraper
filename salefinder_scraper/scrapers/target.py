import requests # Contact api
import json # parse response
import logging
import time
from ..helpers.categories import assign_tags

# Url for department clearance sections, standard is "all" items
def lookup_target_info(department_type, offset):
    target_urls = {
        'baby': f'https://redsky.target.com/v2/plp/search/?category=558or&channel=web&count=96&default_purchasability_filter=true&facet_recovery=false&fulfillment_test_mode=grocery_opu_team_member_test&offset={offset}&pageId=%2Fc%2F558or&pricing_store_id=1102&scheduled_delivery_store_id=12&store_ids=1102%2C1515%2C1279%2C1509%2C1952&visitorId=0173F2D386C60201902922C7D35E81C9&include_sponsored_search_v2=true&ppatok=AOxT33a&platform=desktop&useragent=Mozilla%2F5.0+%28Windows+NT+10.0%3B+Win64%3B+x64%29+AppleWebKit%2F537.36+%28KHTML%2C+like+Gecko%29+Chrome%2F84.0.4147.125+Safari%2F537.36&excludes=available_to_promise_qualitative%2Cavailable_to_promise_location_qualitative&key=ff457966e64d5e877fdbad070f276d18ecec4a01',
        'beauty': f'https://redsky.target.com/v2/plp/search/?category=5514v&channel=web&count=96&default_purchasability_filter=true&facet_recovery=false&fulfillment_test_mode=grocery_opu_team_member_test&offset={offset}&pageId=%2Fc%2F5514v&pricing_store_id=1102&scheduled_delivery_store_id=12&store_ids=1102%2C1515%2C1279%2C1509%2C1952&visitorId=0173F2D386C60201902922C7D35E81C9&include_sponsored_search_v2=true&ppatok=AOxT33a&platform=desktop&useragent=Mozilla%2F5.0+%28Windows+NT+10.0%3B+Win64%3B+x64%29+AppleWebKit%2F537.36+%28KHTML%2C+like+Gecko%29+Chrome%2F84.0.4147.125+Safari%2F537.36&excludes=available_to_promise_qualitative%2Cavailable_to_promise_location_qualitative&key=ff457966e64d5e877fdbad070f276d18ecec4a01',
        'standard': f'https://redsky.target.com/v2/plp/search/?category=5q0ga&channel=web&count=96&default_purchasability_filter=true&facet_recovery=false&fulfillment_test_mode=grocery_opu_team_member_test&offset={offset}&pageId=%2Fc%2F5q0ga&pricing_store_id=1102&visitorId=0173F2D386C60201902922C7D35E81C9&include_sponsored_search_v2=true&ppatok=AOxT33a&platform=desktop&useragent=Mozilla%2F5.0+%28Windows+NT+10.0%3B+Win64%3B+x64%29+AppleWebKit%2F537.36+%28KHTML%2C+like+Gecko%29+Chrome%2F84.0.4147.125+Safari%2F537.36&excludes=available_to_promise_qualitative%2Cavailable_to_promise_location_qualitative&key=ff457966e64d5e877fdbad070f276d18ecec4a01',
        'electronics': {
            'url': f'https://redsky.target.com/v2/plp/search/?category=556x9&channel=web&count=96&default_purchasability_filter=true&facet_recovery=false&fulfillment_test_mode=grocery_opu_team_member_test&offset={offset}&pageId=%2Fc%2F556x9&pricing_store_id=1102&scheduled_delivery_store_id=1102&store_ids=1102%2C1515%2C1279%2C1509%2C1952&visitorId=0173F2D386C60201902922C7D35E81C9&include_sponsored_search_v2=true&ppatok=AOxT33a&platform=mobile&useragent=Mozilla%2F5.0+%28Linux%3B+Android+6.0.1%3B+Moto+G+%284%29%29+AppleWebKit%2F537.36+%28KHTML%2C+like+Gecko%29+Chrome%2F84.0.4147.125+Mobile+Safari%2F537.36&excludes=available_to_promise_qualitative%2Cavailable_to_promise_location_qualitative&key=ff457966e64d5e877fdbad070f276d18ecec4a01',
            'category_type': 'electronics & video games'
        },
        'patio & garden': f'https://redsky.target.com/v2/plp/search/?category=4ua8f&channel=web&count=96&default_purchasability_filter=true&facet_recovery=false&fulfillment_test_mode=grocery_opu_team_member_test&offset={offset}&pageId=%2Fc%2F4ua8f&pricing_store_id=1102&scheduled_delivery_store_id=1102&store_ids=1102%2C1515%2C1279%2C1509%2C1952&visitorId=0173F2D386C60201902922C7D35E81C9&include_sponsored_search_v2=true&ppatok=AOxT33a&platform=desktop&useragent=Mozilla%2F5.0+%28Windows+NT+10.0%3B+Win64%3B+x64%29+AppleWebKit%2F537.36+%28KHTML%2C+like+Gecko%29+Chrome%2F84.0.4147.125+Safari%2F537.36&excludes=available_to_promise_qualitative%2Cavailable_to_promise_location_qualitative&key=ff457966e64d5e877fdbad070f276d18ecec4a01',
        'sports & outdoors': f'https://redsky.target.com/v2/plp/search/?category=54uxx&channel=web&count=96&default_purchasability_filter=true&facet_recovery=false&fulfillment_test_mode=grocery_opu_team_member_test&offset={offset}&pageId=%2Fc%2F54uxx&pricing_store_id=1102&scheduled_delivery_store_id=12&store_ids=1102%2C1515%2C1279%2C1509%2C1952&visitorId=0173F2D386C60201902922C7D35E81C9&include_sponsored_search_v2=true&ppatok=AOxT33a&platform=desktop&useragent=Mozilla%2F5.0+%28Windows+NT+10.0%3B+Win64%3B+x64%29+AppleWebKit%2F537.36+%28KHTML%2C+like+Gecko%29+Chrome%2F84.0.4147.125+Safari%2F537.36&excludes=available_to_promise_qualitative%2Cavailable_to_promise_location_qualitative&key=ff457966e64d5e877fdbad070f276d18ecec4a01',
        'video_games': {
            'url': f'https://redsky.target.com/v2/plp/search/?category=0snqs&channel=web&count=96&default_purchasability_filter=true&facet_recovery=false&fulfillment_test_mode=grocery_opu_team_member_test&offset={offset}&pageId=%2Fc%2F0snqs&pricing_store_id=1102&scheduled_delivery_store_id=1102&store_ids=1102%2C1515%2C1279%2C1509%2C1952&visitorId=0173F2D386C60201902922C7D35E81C9&include_sponsored_search_v2=true&ppatok=AOxT33a&platform=mobile&useragent=Mozilla%2F5.0+%28Linux%3B+Android+6.0.1%3B+Moto+G+%284%29%29+AppleWebKit%2F537.36+%28KHTML%2C+like+Gecko%29+Chrome%2F84.0.4147.125+Mobile+Safari%2F537.36&excludes=available_to_promise_qualitative%2Cavailable_to_promise_location_qualitative&key=ff457966e64d5e877fdbad070f276d18ecec4a01',
            'category_type': 'electronics & video games'
        },
    }

    # Check if type has multiple locations that we need to check
    url = target_urls[department_type]
    has_category_type = False
    if isinstance(url, dict):
        url = url['url']
        has_category_type = True
    elif department_type != 'standard':
        has_category_type = True
    
    # Determine category type
    category_type = 'None'
    if has_category_type:
        if isinstance(target_urls[department_type], dict):
            category_type = target_urls[department_type]['category_type']
        else:
            category_type = department_type

    # Return back target info   
    return [url, has_category_type, category_type]

# Get total amount of items
def target_get_total_pages(url):
    logging.info('Getting target total items')

    api_results = requests.get(url)

    # Could not get results
    if api_results.status_code not in [200, 201]:
        logging.error('Could not contact target api')
        raise RuntimeError('Invalid request to Target')
    
    # Parse results
    parsed_results = json.loads(api_results.text)

    # Go through list of metaData objects, find one with name "totalPages"
    for result in parsed_results['search_response']['metaData']:
        if result['name'] == 'totalPages':
            return int(result['value'])

# Gets all sale items from target and saves them to the database under 'target'
def target(sex):
    # sex parameter is not needed, only for runner function to work with stores that do need it
    logging.info(f'Getting target items')

    # Category chooser for target
    target_categories = {
        '5tg40': 'baby',
        '5xtly': 'baby',
        '54wlm': 'baby',
        '54v3g': 'baby',
        '5xtlp': 'baby',
        '5xtlx': 'baby',
        '5xtk7': 'baby',
        '5tg3w': 'beauty',
        '5tg3c': 'clothing',
        '54ucv': 'clothing',
        '5tg3z': 'electronics & video games',
        '5xtg5': 'electronics & video games',
        '0snqs': 'electronics & video games',
        '4u911': 'electronics & video games',
        '4sppj': 'electronics & video games',
        '4hny0': 'electronics & video games',
        '55mh2': 'electronics & video games',
        'hj96d': 'electronics & video games',
        '55kug': 'electronics & video games',
        'fd2hh': 'electronics & video games',
        'sjng5': 'electronics & video games',
        '1nzwq': 'electronics & video games',
        'vsuh9': 'electronics & video games',
        '5xtfh': 'electronics & video games',
        '54vrp': 'electronics & video games',
        '5xtg5': 'electronics & video games',
        '5xtg6': 'electronics & video games',
        '5xtdw': 'electronics & video games',
        '5xtfc': 'electronics & video games',
        '5xte8': 'electronics & video games',
        '5xteg': 'electronics & video games',
        '5xt9q': 'electronics & video games',
        '551ss': 'electronics & video games',
        '5xtez': 'electronics & video games',
        '5xte2': 'electronics & video games',
        '5xtf0': 'electronics & video games',
        '5xsyz': 'electronics & video games',
        '556x9': 'electronics & video games',
        '5s7dv': 'electronics & video games',
        '4yb7r': 'electronics & video games',
        '556xa': 'electronics & video games',
        '5tg3v': 'home',
        '5tg3x': 'home',
        '54u2m': 'kitchen & dining',
        '5tg3d': 'toys',
        '5xtb0': 'toys',
        '5xt88': 'toys',
        '5xt8r': 'toys',
        '54vro': 'toys',
        '5xtb0': 'toys',
        '1j3un': 'toys',
        '5xt90': 'toys',
        '5xt99': 'toys',
        '5xta7': 'toys',
        '5xt9q': 'toys',
        '5xt9i': 'toys',
        '5xt8k': 'toys',
        '5xsx6': 'toys',
        '5xtai': 'toys',
        '5xt9b': 'toys',
        '5xtap': 'toys',
        '5xtat': 'toys',
        '5xtaz': 'toys',
    }

    # All items list with details
    all_items = []
    offset = 0

    # List of places to check within target website
    departments = [
        'baby',
        'beauty',
        'electronics',
        'patio & garden',
        'sports & outdoors',
        'standard',
        'video_games',
    ]

    # Go through each target sale area; ex. main clearance, video game clearance
    # This is due to target not putting video games on sale in the main clearance area
    for department_type in departments:
        logging.info(f'Get items for Target area: {department_type}')

        # Get url based on department name & page total
        url, has_category_type, category_type = lookup_target_info(department_type, offset)
        page_total = target_get_total_pages(url)

        logging.info(f'{page_total} target page(s) found')
        
        # Grab results from each page
        for page in range(1, page_total):
            logging.info(f'Page {page}: Contacting target api')
            
            url = lookup_target_info(department_type, offset)[0]
            api_results = requests.get(url)
            
            # Could not get results
            if api_results.status_code not in [200, 201]:
                logging.error('Could not contact target api')
                raise RuntimeError('Invalid request to target')
            
            # Parse results
            parsed_results = json.loads(api_results.text)
            items_found = parsed_results['search_response']['items']['Item']

            logging.info(f'Page {page}: Got {len(items_found)} target items')

            logging.info(f'Page {page}: Parsing target items')

            # Loop through raw items list and get details, then add to all_items
            for item in items_found:    
                # Catch any item errors
                if 'error_message' in item:
                    logging.warning(item)
                    continue
                
                # Item details
                try:
                    url = f"https://target.com{item['url']}"
                except:
                    url = 'None'
                
                try:
                    img_url = f"https://target.scene7.com/is/image/Target/{item['images'][0]['primary']}"
                except:
                    img_url = 'None'

                try:
                    brand = item['product_brand']['brand']
                    if brand == '':
                        brand = 'target'
                except:
                    brand = 'None'
                
                try:
                    name = item['title']
                except:
                    name = 'None'
                
                try:
                    subtitle = item['description']
                except:
                    subtitle = 'None'

                try:
                    sale_price = item['price']['current_retail_min']
                except:
                    try:
                        sale_price = item['price']['formatted_current_price']
                        if sale_price == 'See low price in cart':
                            sale_price = 'None'
                        else:
                            sale_price = sale_price.replace('$', '')
                    except:
                        sale_price = 'None'
                
                try:
                    normal_price = item['price']['reg_retail_max']
                except:
                    try:
                        normal_price = item['price']['formatted_comparison_price'].replace('$', '')
                    except:
                        normal_price = 'None'

                # Determine category type
                if not has_category_type:
                    # Standard clearance location, look up category          
                    if 'sales_classification_nodes' in item:
                        for cate_id in item['sales_classification_nodes']:
                            try:
                                category_type = target_categories[cate_id['node_id']]
                            except:
                                pass

                # Assign tags if found a category
                if category_type != 'None':
                    tags = assign_tags([name], category_type=category_type)
                else:
                    tags = []

                # Add item with details to list
                all_items.append({
                    'url': url,
                    'img_url': img_url,
                    'brand': brand,
                    'name': name,
                    'sex': 'N/A',
                    'sale_price': sale_price,
                    'normal_price': normal_price,
                    'type': category_type,
                    'tags': tags,
                })
            
            logging.info(f'Page {page}: Done!')

            # Set next page
            offset += 96
            time.sleep(.117)

        # Reset offset for next area
        logging.info(f'Done with Target area - {department_type}')
        offset = 0

    logging.info(f'Total target Items: {len(all_items)}')
    
    # Send back all products
    return all_items