import os
import requests
import logging
import time
import re
from bs4 import BeautifulSoup
from ..helpers.categories import assign_tags

# Get total of items on sale to determine pages to run
def get_total_items(sex):
    logging.info('Getting items count from Ralph Lauren')

    if sex == 'male':
        url = f'https://www.ralphlauren.com/men/sale?start=0&sz=4'
    elif sex == 'female':
        url = f'https://www.ralphlauren.com/women/sale?start=0&sz=4'
    else:
        raise Exception('Invalid selection for sex, must be male or female')

    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
        'path': '/sale-women-collection-sale-cg?start=7&sz=4&format=ajax',
        'referer': url,
        'cookie': '__cfduid=db400b03c5704ebc7544ed25529c983501597116992; cqcid=abVyjiHA0acBf5NT1D9FdvgWzq; dwanonymous_55b6a3b329e729876c1d594e39f4ac4e=abVyjiHA0acBf5NT1D9FdvgWzq; pzcookie="{\"EP_RID\":\"\",\"gender\":0}"; __cq_dnt=0; dw_dnt=0; AMCVS_F18502BE5329FB670A490D4C%40AdobeOrg=1; dw=1; dw_cookies_accepted=1; __cfduid=d81e1055630f9663044a5581bc5b918c61597116997; modalLoad=1; headerDefault=1; dwac_102c95db27e6f188d36d6303ba=-178vEqy2Mp-JGc9gMXDoJpyP87eUDuK8eM%3D|dw-only|||USD|false|US%2FEastern|true; sid=-178vEqy2Mp-JGc9gMXDoJpyP87eUDuK8eM; dwsid=_VhKRBDzN_11x0e1YwcG0Zx0iTaEEdS9-gBeedrjo__d1MW4rq2cW5ckrD5_8GG8sfJVYV4bsAJYa6Acw0bifA==; pageNameDuplicate=en_US%3Aplp%3AWomen%20s%20Designer%20Sale%3A%20Up%20to%2060%25%20Off%3Asale%20women%20collection%20sale%20cg; AMCV_F18502BE5329FB670A490D4C%40AdobeOrg=-408604571%7CMCIDTS%7C18486%7CMCMID%7C44532787643907039509206298285763618359%7CMCAID%7CNONE%7CMCOPTOUT-1597156164s%7CNONE%7CvVersion%7C4.6.0; OptanonConsent=isIABGlobal=false&datestamp=Tue+Aug+11+2020+07%3A29%3A25+GMT-0500+(Central+Daylight+Time)&version=5.11.0&landingPath=NotLandingPage&groups=C0005%3A0%2CC0002%3A0%2CC0004%3A0%2CBG5%3A0%2CC0003%3A1%2CC0001%3A1&hosts=&AwaitingReconsent=false; forterToken=bba07acbce7246cea5b9ef4c228c588f_1597148965321_578_UAL9_6',
        'dnt': '1',
        'accept': 'text/html, */*; q=0.01',
        'accept-language': 'en-US,en;q=0.9',
        'x-requested-with': 'XMLHttpRequest',
    }
    page_results = requests.get(url, headers=headers)

    # Could not get results from ralph lauren
    if page_results.status_code not in [200, 201]:
        logging.info('Could not contact ralphlauren.com')
        raise Exception(page_results)

    # Get list of items
    soup = BeautifulSoup(page_results.text, features="lxml")
    total_items = soup.find('div', { 'class': 'results-hits' }).text.split('of')[1].split('Items')[0].replace('\n', '').replace('\t', '').replace('\r', '').replace(',','').strip()
    return total_items

# Gets all sale items from ralph lauren and saves them to the database under 'ralphLauren'
def ralph_lauren(sex):
    logging.info(f'Getting {sex} Ralph Lauren items')
    logging.info('Contacting Ralphlauren.com')

    # Gets amount of items, helps us determine amount of pages
    total_items = get_total_items(sex)
    cycles = (int(total_items) // 32) + 2
    start = 0

    logging.info(f'Total items: {total_items}')
    logging.info(f'Total cycles: {cycles}')    
    all_items = []

    for cycle in range(1, cycles):
        logging.info(f'Starting cycle {cycle}')
        logging.info('Contacting ralphlauren.com')

        if sex == 'male':
            url = f'https://www.ralphlauren.com/men/sale?start={start}&sz=32'
        elif sex == 'female':
            url = f'https://www.ralphlauren.com/women/sale?start={start}&sz=32'
        else:
            raise Exception('Invalid selection for sex, must be male or female')


        # Request details
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
            'path': '/sale-women-collection-sale-cg?start=7&sz=4&format=ajax',
            'referer': url,
            'cookie': '__cfduid=db400b03c5704ebc7544ed25529c983501597116992; cqcid=abVyjiHA0acBf5NT1D9FdvgWzq; dwanonymous_55b6a3b329e729876c1d594e39f4ac4e=abVyjiHA0acBf5NT1D9FdvgWzq; pzcookie="{\"EP_RID\":\"\",\"gender\":0}"; __cq_dnt=0; dw_dnt=0; AMCVS_F18502BE5329FB670A490D4C%40AdobeOrg=1; dw=1; dw_cookies_accepted=1; __cfduid=d81e1055630f9663044a5581bc5b918c61597116997; modalLoad=1; headerDefault=1; dwac_102c95db27e6f188d36d6303ba=-178vEqy2Mp-JGc9gMXDoJpyP87eUDuK8eM%3D|dw-only|||USD|false|US%2FEastern|true; sid=-178vEqy2Mp-JGc9gMXDoJpyP87eUDuK8eM; dwsid=_VhKRBDzN_11x0e1YwcG0Zx0iTaEEdS9-gBeedrjo__d1MW4rq2cW5ckrD5_8GG8sfJVYV4bsAJYa6Acw0bifA==; pageNameDuplicate=en_US%3Aplp%3AWomen%20s%20Designer%20Sale%3A%20Up%20to%2060%25%20Off%3Asale%20women%20collection%20sale%20cg; AMCV_F18502BE5329FB670A490D4C%40AdobeOrg=-408604571%7CMCIDTS%7C18486%7CMCMID%7C44532787643907039509206298285763618359%7CMCAID%7CNONE%7CMCOPTOUT-1597156164s%7CNONE%7CvVersion%7C4.6.0; OptanonConsent=isIABGlobal=false&datestamp=Tue+Aug+11+2020+07%3A29%3A25+GMT-0500+(Central+Daylight+Time)&version=5.11.0&landingPath=NotLandingPage&groups=C0005%3A0%2CC0002%3A0%2CC0004%3A0%2CBG5%3A0%2CC0003%3A1%2CC0001%3A1&hosts=&AwaitingReconsent=false; forterToken=bba07acbce7246cea5b9ef4c228c588f_1597148965321_578_UAL9_6',
            'dnt': '1',
            'accept': 'text/html, */*; q=0.01',
            'accept-language': 'en-US,en;q=0.9',
            'x-requested-with': 'XMLHttpRequest',
        }
        page_results = requests.get(url, headers=headers)

        # Could not get results from ralph lauren
        if page_results.status_code not in [200, 201]:
            logging.warn('Could not contact ralph lauren.com')
            raise Exception(page_results)
    
        logging.info('Got ralph lauren items')

        # Get list of items
        soup = BeautifulSoup(page_results.text, features="lxml")
        item_container = soup.find('div', { 'id': 'search-result-items' })
        raw_items_list = item_container.findChildren('div', { 'class': 'grid-tile' })

        logging.info('Parsing ralph lauren items')
        # All items list with details, example:
        # 'blue_shoe': {
        #     'price': 12.40,
        #     'img_url': 'blahblah.img',
        #     'item_url': 'http://ralph lauren.com/blue-shoe'
        # }
        
        # Loop through raw items list and get details, then add to all_items
        for item in raw_items_list:
            # Get item details and clean up if we found the detail 
            try:
                url = f"https://ralphlauren.com{item.find('a', class_='thumb-link')['href']}"
            except:
                url = 'None'
            
            try:
                img_url = item.find('img', class_='default-img')['src']
            except:
                img_url = 'None'
            
            try:
                name = item.find('div', { 'class': 'product-name' }).getText().replace('\n', '').replace('\t', '').replace('\r', '').replace('$', '').strip()
            except:
                name = 'None'

            
            pricing_container = item.find('div', class_='product-pricing').text.split('$')

            try:
                # sale_price = pricing_container[2].replace('\n', '').spl.replace(',','').strip()
                sale_price = re.sub(r'[^0-9.]+', '', pricing_container[2]).replace('\n', '').strip()
            except:
                sale_price = 'None'
            
            try:
                # normal_price = pricing_container[1].replace('Sale Price', '').replace(',', '').strip()
                normal_price = re.sub(r'[^0-9.]+', '', pricing_container[1]).replace('\n', '').strip()
            except:
                normal_price = 'None'

            # Assign tags
            tags = assign_tags(name, category_type="clothing")

            # Add item with details to list
            all_items.append({
                'url': url,
                'img_url': img_url,
                'brand': 'Ralph Lauren',
                'name': name,
                'sex': sex,
                'sale_price': sale_price,
                'normal_price': normal_price,
                'type': 'clothing',
                'tags': tags,
            })

        # Adjust offset
        start += 32

        # Cooldown to prevent 403 error if not last cycle
        if cycle != cycles:
            logging.info('Waiting before contacting Ralph Lauren again')
            time.sleep(5)

    return all_items
