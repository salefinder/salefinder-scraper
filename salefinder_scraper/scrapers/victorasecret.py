import requests # Contact api
import json # parse response
import logging
from ..helpers.categories import assign_tags

# Gets all sale items and saves them to the database under 'victoriaSecret'
def victoria_secret(sex):
    logging.info('Getting Victorias Secret womens items')
    logging.info('Contacting Victorias Secret api')

    url = 'https://api.victoriassecret.com/stacks/v8/stack?collectionId=091f2401-7b37-42cb-8f74-efc3b6264707&stackId=ab9b4799-ed6c-47dc-8bb4-855a29ad7103&orderBy=REC&limit=1000&activeCountry=US'
    api_results = requests.get(url)
    
    # Could not get results
    if api_results.status_code not in [200, 201]:
        logging.error('Could not contact Victorias secret api')
        raise Exception('Invalid request to Victorias secret')
    
    # Parse results
    parsed_results = json.loads(api_results.text)

    logging.info('Got Victorias Secret items')

    logging.info('Parsing Victorias Secret items')

    # All items list with details
    all_items = []

    # Check if any errors
    if len(parsed_results) == 0:
        raise(parsed_results)

    # Loop through raw items list and get details, then add to all_items
    for item in parsed_results:        
        # Item details
        url = f"https://www.victoriassecret.com{item['url']}"
        img_url = f'https://www.victoriassecret.com/p/280x373/{item["productImages"][0]}.jpg'
        brand = "Victoria's Secret"
        name = item['name']
        sale_price = item['salePrice'].strip('$')
        normal_price = item['price'].strip('$')

        # Assign tags
        tags = assign_tags(name, category_type="clothing")
        
        # Add item with details to list
        all_items.append({
            'url': url,
            'img_url': img_url,
            'brand': brand,
            'sex': 'female',
            'name': name,
            'sale_price': sale_price,
            'normal_price': normal_price,
            'type': 'clothing',
            'tags': tags,
        })

    return all_items