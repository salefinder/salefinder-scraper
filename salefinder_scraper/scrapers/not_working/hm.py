from bs4 import BeautifulSoup
import requests
from ..helpers.mongohelpers import insert_documents

# Gets all sale items from ulta and saves them to the database under 'ulta'
def hm_women():
    print('Getting H&M items')
    print('Contacting hm.com')

    # Request details
    # Women's sale items
    website_url = 'https://www2.hm.com/en_us/sale/women/view-all.html?sort=stock&image-size=small&image=stillLife&offset=0&page-size=10000'
    headers = { 'User-Agent': 'Mozilla/5.0' }
    page_results = requests.get(website_url, headers=headers)
    
    # Could not get results from ulta
    if page_results.status_code not in [200, 201]:
        print('Could not contact hm.com')
        error = {
            'status_code': page_results.status_code,
            'message': page_results.text
        }

        print('Invalid request to HM.com')
        raise Exception(error)
    
    print('Got H&M items')

    # Get list of items
    soup = BeautifulSoup(page_results.text, features="lxml")
    item_container = soup.find('ul', { 'class': 'products-listing small' })
    raw_items_list = item_container.findChildren('li')

    print('Parsing H&M items')
    # All items list with details, example:
    # 'blue_shoe': {
    #     'price': 12.40,
    #     'img_url': 'blahblah.img',
    #     'item_url': 'http://hm.com/blue-shoe'
    # }
    all_items = []
    
    # Loop through raw items list and get details, then add to all_items
    for item in raw_items_list:
        # Get item details
        url = item.find('a')
        img_url = item.find('img')
        name = item.find('h3', { 'class': 'item-heading' })
        sale_price = item.find('span', { 'class': 'price sale' })
        normal_price = item.find('span', { 'class': 'price regular' })

        if url is not None:
            try:
                url = url['href']
            except:
                url = 'None'
        else:
            url = 'None'
        
        if img_url is not None:
            try:
                img_url = img_url['src']
            except:
                img_url = 'None'
        else:
            img_url = 'None'
        
        if name is not None:
            try:
                name = name.getText().replace('\n', '').replace('\t', '').strip()
            except:
                name = 'None'
        else:
            name = 'None'

        if sale_price is not None:
            try:
                sale_price = sale_price.text.replace('\n', '').replace('\t', '').replace('\r', '').strip()
            except:
                sale_price = 'None'
        else:
            sale_price = 'None'

        if normal_price is not None:
            try:
                normal_price = normal_price.text.replace('\n', '').replace('\t', '').replace('\r', '').strip()
            except:
                normal_price = 'None'
        else:
            normal_price = 'None'

        # Add item with details to list
        all_items.append({
            'url': f'{website_url}{url}',
            'img_url': img_url,
            'name': name,
            'sale_price': sale_price,
            'normal_price': normal_price
        })
    
    print('Saving H&M items to database')

    # Insert items into database
    results = insert_documents('HM', all_items)
    return results