from selenium import webdriver
import time

# Gets all sale items from urbanOutfitters and saves them to the database under 'urbanOutfitters'
def urbanOutfitters_women(page_number, all_items):
    print(f'Collecting products from page {page_number}')

    # Selenium to open window so we can get HTML, since it's javascript rendered
    url = f'https://www.urbanoutfitters.com/womens-clothing-sale?sort=tile.product.newestColorDate&order=Descending&page={page_number}'
    browser = webdriver.Chrome(os.getenv('CHROMEDRIVER_PATH'))
    browser.get(url)
    
    # Give page time to load all items
    time.sleep(5)

    # Find all items, add to total items list
    item_container = browser.find_element_by_class_name('c-pwa-tile-tiles')
    raw_items_list = item_container.find_elements_by_class_name("c-pwa-tile-grid-inner")

    print(f'Page {page_number}: parsing items')
    
    # Loop through raw items list and get details, then add to all_items
    for item in raw_items_list:
        # Get item details
        try:
            url = item.find_element_by_tag_name('a').get_attribute('href')
        except:
            url = 'None'

        try:
            img_url = item.find_element_by_tag_name('img').get_attribute('src')
        except:
            img_url = 'None'
        
        try:
            name = item.find_element_by_class_name('c-pwa-product-tile__heading').text
        except:
            name = 'None'
        
        try:
            sale_price = item.find_element_by_class_name('c-pwa-product-price__current c-pwa-product-price__current--sale-temporary').text
        except:
            try:
                sale_price = item.find_element_by_class_name('c-pwa-product-price__current').text
            except:
                sale_price = 'None'
        
        try:
            normal_price = item.find_element_by_class_name('c-pwa-product-price__original').text
        except:
            try:
                normal_price = item.find_element_by_class_name('c-pwa-product-promos').text
            except:
                normal_price = 'None'

        # Add item with details to list
        all_items.append({
            'url': url,
            'img_url': img_url,
            'name': name,
            'sale_price': sale_price,
            'normal_price': normal_price
        })
    
    # Quit the window
    browser.quit()

    print(f'Page {page_number} done')
    return
