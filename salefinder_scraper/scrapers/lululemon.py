import requests # Contact api
import json # parse response
import logging
from ..helpers.categories import assign_tags

# Gets all sale items from ulta and saves them to the database under 'ulta'
def lululemon(sex):
    logging.info(f'Getting Lululemon {sex} items')
    logging.info('Contacting Lululemon api')

    # Lululemon is nice enough to have an unsecured api, lets use it
    cookies = {
        "ltmo": "1",
        "lll-ecom-correlation-id": "50C0926B-3874-F72E-7848-5E8BE69BE8E4",
        "check": "true",
        "AMCVS_A92B3BC75245B1030A490D4D%40AdobeOrg": "1",
        "s_ecid": "MCMID%7C26370906723697038632961708935178518344",
        "_abck": "5AEBD2BE2B92DD18AD31D7157F659BC5~0~YAAQHcLFF4Yol/FyAQAA5+HC9wQqyNVthqzT19QbYbDAzRfMquAeucsa56JOXKYs0KYSIMP67EbuCYJ+tpNrwe7Ndd60ZMLMjc9ZMUbnWT6qEEEFmgP/3+3wT4kWCYFY9+s6SMra2SW+MZAXHo4fGxoGkQ9+8YPb2193FCaqJWhu1zdLTfF3YVDkL+5lfwkkxA1Vkcb0OyiOosanhsLPiiniYkILM6PrdEy91GY7NJBTF2mfP7mREF25rovGaKkRHExCuK9QAKKefJc6pitWQQHR4TPeoT8VBdYe+3EofzENws9HdTaSBkQAzKdV/zDYuHDHizUe+tzRVw==~-1~-1~-1",
        "lll_adobe_geo_state": "missouri",
        "isLoggedin": "false",
        "WLS_ROUTE": ".b-03",
        "_gcl_au": "1.1.1293156702.1593294645",
        "LNM_LoginOnly_MVP": "false",
        "enableCovidMessage": "false",
        "fusion-pdp-exp": "itemRec_us:e8b9e81144",
        "fusion-cdp-exp": "IgnoreExmUS:12bd6ebd12",
        "undefined": "IgnoreExmUS:12bd6ebd12",
        "kampyle_userid": "13bd-f43a-4a56-99a2-0135-053b-51ae-d331",
        "cd_user_id": "172f7c2eaa417c-04aaaad906b829-39770e5a-2a3000-172f7c2eaa57c9",
        "QuantumMetricUserID": "6d57e6966fa10f11576e03a0ad48b677",
        "QuantumMetricSessionID": "4c457765042a306a08477b1c1a2edaf2",
        "xdVisitorId": "12B8EU55JPKDpWVwKdiqxt8FFbOlbyNAmFAKsaYH8yvkzHYDD64",
        "atgRecVisitorId": "12B8EU55JPKDpWVwKdiqxt8FFbOlbyNAmFAKsaYH8yvkzHYDD64",
        "s_cc": "true",
        "_fbp": "fb.1.1593294646203.1529368865",
        "a1ashgd": "uiz217sufg000000uiz217sufg000000",
        "bm_sz": "E53BE14CFD6E7BE784A9FA6E05382254~YAAQFk9yaKvpD4NzAQAA4xlunQj2m2c8KtSxajp+ZaAWF/DVLUPXr3OGm75TWnszfxaotFod3cTKL0nRDz1+0/KBPuVp7H0sY1yGE8HzcT5hKKii4TVGiim3WZA9YXWRGTFrrG7HBkLwgXGWWHPp/ObyvsysmeSv6pEkpaql5biR+yfQWhIiRQtRswCbT2IA/cJL",
        "UsrLocale": "en_US",
        "Country": "US",
        "AMCV_A92B3BC75245B1030A490D4D%40AdobeOrg": "870038026%7CMCIDTS%7C18474%7CMCMID%7C26370906723697038632961708935178518344%7CMCAAMLH-1596678907%7C7%7CMCAAMB-1596678907%7CRKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y%7CMCOPTOUT-1596081307s%7CNONE%7CMCAID%7CNONE%7CvVersion%7C5.0.0",
        "ak_bmsc": "3399D92D11E8C0AA46E651DE20CB9CAE68724F563C5300007928225FA01D304F~plbO5rEvnRsM5Fznf1+6aA83wuHMd38JB26SjKTm5eSeCUGqGvcC5nZzzQvHEPgSoaJcPzhDjf98esdxjFcy7RVuz7V1Tmi3wQl08UPH+NCaDjagE/emzYG248S8q158WK/hO/icRu95FryvXDrFEx+t+ld1gGi7TyHtF09JZADmLij7MVEfoQgJ0+E3KnUnnxTLdnJ6Mc3xpLTpNlBjULcLjazKjD2amSJmigJQfZK+lPri+LtZO2EbPkHqi+mQoy",
        "lll_adobe_geo_country": "united%20states",
        "lll_adobe_geo_city": "st%20louis",
        "lll_adobe_geo_postal_zip": "63129",
        "lll_adobe_geo_latitude": "38.46",
        "lll_adobe_geo_longitude": "-90.33",
        "digitalData.page.a1Token": "$2a$10$clmStjkHFVYfNp47hRaZL.nH4l7BS/lCVrgMq9y582XeMMh7JprMm",
        "sl": "US",
        "cartCount": "0",
        "_gid": "GA1.2.1433815498.1596074111",
        "optimizeGoogle": "abt-663-md-onTheMove-vs-lifestyle-v1",
        "addToBagLowStockNotification": "true",
        "_gaexp": "GAX1.2.7eQ3bKsDQQajdJry2V_PUA.18551.2!vSF2I_DjRGiOvg9Dfmm4QA.18556.2!y0So4vIFRrS7nuO3tu3cMQ.18563.2",
        "pdpThumbnails": "true",
        "JSESSIONID": "6ludjxfes3JwJM7XwDh0F4CK8h2kU1oyK7qkvyLKu6IH_8YDdJQ3!330253038",
        "us_ord": "\"pcBi4GvCJb3b226h2Y6wdw==\"",
        "us_ord_stores": "\"pcBi4GvCJb3b226h2Y6wdw==<|>XzGjq9zxQ8E=\"",
        "atgRecSessionId": "lVGdjx0L-yLYT5SZk_4ZcHHLyQ5A2J0Tkl48kOEIB2fjSNKTmATH!1905807844!-1515672714",
        "mbox": "PC#d50d2547fd6843429680b8494e99c691.34_0#1659323104|session#efd200a238df45a09bff485ac05cf360#1596080102",
        "bm_sv": "AAE01A681D751A27D681E5B83EC908DD~7fnh4lijhuEwHCNscgGzDkz80qLn1Hy9xkVztXnmRQr9A1ZDSTh8pwuNl9qLhEfoPXT2fspSMeN/7O3Oz1Zn8rkJkPkYJ1bAvDgXtORzjP8ywNHffmzKC/qTeytVy6PEWwaIEROz69ruf6ZRDMztH4piY5wUbmNGgIYii2nZG40=",
        "_uetsid": "660e37eba35cc26c02fffc1764bcb198",
        "_uetvid": "fcf3179741f895469db67a40b2460475",
        "_ga_CCD7VVYPZ7": "GS1.1.1596076268.3.1.1596078304.4",
        "kampyleUserSession": "1596078304643",
        "kampyleUserSessionsCount": "13",
        "_ga": "GA1.2.1643599857.1593294645",
        "kampyleSessionPageCounter": "2",
        "s_sq": "llmnprod%3D%2526c.%2526a.%2526activitymap.%2526page%253Dsearch%25253Asearch-results%2526link%253DView%252520more%252520products%2526region%253Dmain-content%2526pageIDType%253D1%2526.activitymap%2526.a%2526.c%2526pid%253Dsearch%25253Asearch-results%2526pidt%253D1%2526oid%253Dfunction%252524n%252528%252529%25257B%25257D%2526oidt%253D2%2526ot%253DBUTTON"
    }
    products_amount = 300

    if sex == 'male':
        url = f'https://shop.lululemon.com/api/s?Ntt=men%2Bsale&ta=1&tasid=3LQ1vRxIFp&taterm=sale&page[offset]=0&page[limit]={products_amount}&session=50C0926B-3874-F72E-7848-5E8BE69BE8E4&userId=MCMID%7C26370906723697038632961708935178518344'
    else:
        url = f'https://shop.lululemon.com/api/s?Ntt=women%2Bsale&ta=1&tasid=3LQ1vRxIFp&taterm=sale&page[offset]=0&page[limit]={products_amount}&session=50C0926B-3874-F72E-7848-5E8BE69BE8E4&userId=MCMID%7C26370906723697038632961708935178518344'
    
    api_results = requests.get(url, cookies=cookies)
    
    # Could not get results
    if api_results.status_code not in [200, 201]:
        logging.error('Could not contact lululemon api')
        raise Exception('Invalid request to Lululemon')
    
    # Parse results
    parsed_results = json.loads(api_results.text)

    logging.info('Got lululemon items')

    logging.info('Parsing lululemon items')

    # All items list with details
    all_items = []
    
    # Loop through raw items list and get details, then add to all_items
    for item in parsed_results['included']:

        # Check for end of items, exit loop
        if item['type'] != 'products':
            break
        
        # Item details
        url = f'https://shop.lululemon.com{item["pdp-url"]}'
        img_url = item['sku-sku-images'][0]
        brand = 'Lululemon'
        name = item['title'].split('|')[0].replace('\n', '').replace('\t', '').strip()
        sale_price = item['product-sale-price'][0]
        normal_price = item['list-price'][0]

        # Assign tags
        tags = assign_tags(name, category_type="clothing")
        
        # Add item with details to list
        all_items.append({
            'url': url,
            'img_url': img_url,
            'brand': brand,
            'name': name,
            'sex': sex,
            'sale_price': sale_price,
            'normal_price': normal_price,
            'type': 'clothing',
            'tags': tags,
        })

    return all_items
