import requests
import logging
from bs4 import BeautifulSoup
from ..helpers.categories import assign_tags

# Gets all sale items from ulta and saves them to the database under 'ulta'
def ulta(sex):
    logging.info('Getting ulta items')
    logging.info('Contacting ulta.com')

    # Request details
    if sex == 'female':
        url = 'https://www.ulta.com/promotion/sale?Nrpp=5000'
    else:
        raise Exception('Invalid selection for sex, must be male or female')
    
    page_results = requests.get(url)

    # Could not get results from ulta
    if page_results.status_code not in [200, 201]:
        logging.info('Could not contact ulta.com')
        raise Exception('Invalid request to ulta.com')
    
    logging.info('Got ulta items')

    # Get list of items
    soup = BeautifulSoup(page_results.text, features="lxml")
    item_container = soup.find(id="product-category-cont")
    raw_items_list = item_container.findChildren("li")

    logging.info('Parsing ulta items')
    # All items list with details, example:
    # 'blue_shoe': {
    #     'price': 12.40,
    #     'img_url': 'blahblah.img',
    #     'item_url': 'http://ulta.com/blue-shoe'
    # }
    all_items = []
    
    # Loop through raw items list and get details, then add to all_items
    for item in raw_items_list:
        # Get item details and clean up if we found the detail 
        try:
            url = f"https://www.ulta.com{item.find('a')['href']}"
        except:
            url = 'None'
        
        try:
            img_url = item.find_all('img')[1]['src']
        except:
            img_url = 'None'

        try:
            brand = item.find('h4', { 'class': 'prod-title' })
            brand = brand.getText().replace('\n', '').replace('\t', '').strip()
        except:
            brand = 'None'
        
        try:
            name = item.find('p', { 'class': 'prod-desc' })
            name = name.getText().replace('\n', '').replace('\t', '').strip()
        except:
            name = 'None'

        try:
            sale_price = item.find('span', { 'class': 'pro-new-price' })
            sale_price = sale_price.text.replace('\n', '').replace('\t', '').replace('\r', '').replace('$', '').strip()
        except:
            sale_price = 'None'

        try:
            normal_price = item.find('span', { 'class': 'pro-old-price' })
            normal_price = normal_price.text.replace('\n', '').replace('\t', '').replace('\r', '').replace('$', '').strip()
        except:
            normal_price = 'None'

        # Assign tags
        tags = assign_tags(name, category_type="beauty")

        # Add item with details to list
        all_items.append({
            'url': url,
            'img_url': img_url,
            'brand': brand,
            'sex': 'female',
            'name': name,
            'sale_price': sale_price,
            'normal_price': normal_price,
            'type': 'beauty',
            'tags': tags,
        })

    return all_items