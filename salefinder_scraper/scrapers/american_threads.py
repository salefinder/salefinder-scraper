import os
import time
import logging
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from ..helpers.categories import assign_tags

# Gets all sale items from american threads and saves them to the database under 'american threads'
def american_threads(sex, page_number, all_items):
    logging.info(f'Page {page_number} {sex}: Launching controlled selenium window...')
    
    if sex == 'female':
        # Selenium to open window so we can get HTML, since it's javascript rendered
        url = f'https://www.shopamericanthreads.com/collections/sale?page={page_number}&sort_by=created-descending'
    else:
        raise Exception('Invalid selection for sex, must be male or female')
    
    options = Options()
    options.headless = True
    options.add_argument('--no-sandbox')
    options.add_argument('--window-size=1420,1080')
    options.add_argument('disable-gpu')
    browser = webdriver.Chrome(os.getenv('CHROMEDRIVER_PATH'), chrome_options=options)
    browser.get(url)

    logging.info(f'Page {page_number}: window has been launched')
    
    # Give page time to load all items
    logging.info(f'Page {page_number}: Waiting 5 seconds for page to load...')
    time.sleep(5)

    logging.info(f'Page {page_number}: Locating products on page')

    # Find all items, add to total items list
    item_container = browser.find_element_by_xpath('//*[@id="PageContainer"]/div[1]/div[2]/div[1]/div[3]')
    raw_items_list = item_container.find_elements_by_class_name('item')

    logging.info(f'Page {page_number}: items located!')
    logging.info(f'Page {page_number}: parsing items')
    
    # Loop through raw items list and get details, then add to all_items
    for item in raw_items_list:
        # Get item details
        try:
            url = item.find_element_by_tag_name('a').get_attribute('href')
        except:
            url = 'None'

        try:
            img_url = item.find_element_by_class_name('img-cnt').get_attribute('style')
            # Grab url from messy bullshit they do
            img_url = img_url.split('//')[1].split('")')[0]
        except:
            img_url = 'None'
        
        try:
            name = item.find_element_by_tag_name('p').text
        except:
            name = 'None'
        
        try:
            sale_price = item.find_element_by_class_name('originalPrice').text.strip('$').replace(' ', '')
        except:
            sale_price = 'None'
        
        try:
            normal_price = item.find_element_by_class_name('salePrice').text.strip('$').replace(' ', '')
        except:
            normal_price = 'None'

        # Assign tags
        tags = assign_tags(name, category_type="clothing")

        # Add item with details to list
        all_items.append({
            'url': url,
            'img_url': img_url,
            'name': name,
            'sex': 'female',
            'brand': 'americanThreads',
            'sale_price': sale_price,
            'normal_price': normal_price,
            'type': 'clothing',
            'tags': tags,
        })
    
    # Quit the window
    browser.quit()

    logging.info(f'Page {page_number}: thread is done')
    return
