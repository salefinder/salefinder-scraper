import requests # Contact api
import json # parse response
import logging
import time
from ..helpers.categories import assign_tags

# Get total amount of items
def nike_get_total_pages(sex):
    logging.info('Getting Nike total items')

    if sex == 'female':
        url = 'https://api.nike.com/cic/browse/v1?queryid=filteredProductsWithContext&anonymousId=75371888AFAFA827F97E34842B2491A6&uuids=5b21a62a-0503-400c-8336-3ccfbff2a684,7baf216c-acc6-4452-9e07-39c2ca77ba32&language=en&country=US&channel=NIKE&localizedRangeStr=%7BlowestPrice%7D%20%E2%80%94%20%7BhighestPrice%7D&sortBy=newest'
    elif sex == 'male':
        url = 'https://api.nike.com/cic/browse/v1?queryid=products&anonymousId=75371888AFAFA827F97E34842B2491A6&country=us&endpoint=%2Fproduct_feed%2Frollup_threads%2Fv2%3Ffilter%3Dmarketplace(US)%26filter%3Dlanguage(en)%26filter%3DemployeePrice(true)%26filter%3DattributeIds(0f64ecc7-d624-4e91-b171-b83a03dd8550%2C5b21a62a-0503-400c-8336-3ccfbff2a684)%26anchor%3D48%26consumerChannelId%3Dd9a5bc42-4b9c-4976-858a-f159cf99c647%26count%3D24%26sort%3DeffectiveStartViewDateDesc&language=en&localizedRangeStr=%7BlowestPrice%7D%20%E2%80%94%20%7BhighestPrice%7D'
    else:
        raise KeyError('Invalid selection for sex, must be male or female')

    api_results = requests.get(url)

    # Could not get results
    if api_results.status_code not in [200, 201]:
        logging.error('Could not contact nike api')
        raise RuntimeError('Invalid request to Nike')
    
    # Parse results
    parsed_results = json.loads(api_results.text)

    if sex == 'female':
        return parsed_results['data']['filteredProductsWithContext']['pages']['totalPages']
    else:
        return parsed_results['data']['products']['pages']['totalPages']
        
# Gets all sale items from nike and saves them to the database under 'nike'
def nike(sex):
    logging.info(f'Getting Nike {sex} items')

    page_total = nike_get_total_pages(sex)
    logging.info(f'{page_total} Nike page(s) found')

    # All items list with details
    all_items = []
    anchor = 0

    # Grab results from each page
    for page in range(1, page_total):
        logging.info(f'Page {page}: Contacting Nike api')

        if sex == 'female':
            url = f'https://api.nike.com/cic/browse/v1?queryid=products&anonymousId=75371888AFAFA827F97E34842B2491A6&country=us&endpoint=%2Fproduct_feed%2Frollup_threads%2Fv2%3Ffilter%3Dmarketplace(US)%26filter%3Dlanguage(en)%26filter%3DemployeePrice(true)%26filter%3DattributeIds(5b21a62a-0503-400c-8336-3ccfbff2a684%2C7baf216c-acc6-4452-9e07-39c2ca77ba32)%26anchor%3D{anchor}%26consumerChannelId%3Dd9a5bc42-4b9c-4976-858a-f159cf99c647%26count%3D24%26sort%3DeffectiveStartViewDateDesc&language=en&localizedRangeStr=%7BlowestPrice%7D%20%E2%80%94%20%7BhighestPrice%7D'
        elif sex == 'male':
            url = f'https://api.nike.com/cic/browse/v1?queryid=products&anonymousId=75371888AFAFA827F97E34842B2491A6&country=us&endpoint=%2Fproduct_feed%2Frollup_threads%2Fv2%3Ffilter%3Dmarketplace(US)%26filter%3Dlanguage(en)%26filter%3DemployeePrice(true)%26filter%3DattributeIds(0f64ecc7-d624-4e91-b171-b83a03dd8550%2C5b21a62a-0503-400c-8336-3ccfbff2a684)%26anchor%3D{anchor}%26consumerChannelId%3Dd9a5bc42-4b9c-4976-858a-f159cf99c647%26count%3D24&language=en&localizedRangeStr=%7BlowestPrice%7D%20%E2%80%94%20%7BhighestPrice%7D'
        else:
            raise KeyError('Invalid selection for sex, must be male or female')
        
        api_results = requests.get(url)
        
        # Could not get results
        if api_results.status_code not in [200, 201]:
            logging.error('Could not contact nike api')
            raise RuntimeError('Invalid request to Nike')
        
        # Parse results
        parsed_results = json.loads(api_results.text)

        logging.info(f'Page {page}:Got nike items')

        logging.info(f'Page {page}: Parsing nike items')

        # Check if any errors
        if parsed_results['data']['products']['errors']:
            raise RuntimeError(parsed_results['data']['products']['errors'])

        # Loop through raw items list and get details, then add to all_items
        for item in parsed_results['data']['products']['products']:        
            # Item details
            url = f"https://nike.com/{item['url'].split('/', 1)[1]}"
            img_url = item['images']['portraitURL']
            brand = 'Nike'
            name = item['title'].split('|')[0].replace('\n', '').replace('\t', '').strip()
            subtitle = item['subtitle']
            sale_price = item['price']['currentPrice']
            normal_price = item['price']['fullPrice']
            
            # Assign tags
            tags = assign_tags([name, subtitle], category_type="clothing")

            # Add item with details to list
            all_items.append({
                'url': url,
                'img_url': img_url,
                'brand': brand,
                'name': name,
                'sex': sex,
                'sale_price': sale_price,
                'normal_price': normal_price,
                'type': 'clothing',
                'tags': tags,
            })
        
        logging.info(f'Page {page}: Done!')

        # Set next page
        anchor += 24
        time.sleep(.15)

    logging.info(f'Total Nike Items: {len(all_items)}')
    
    # Send back all products
    return all_items