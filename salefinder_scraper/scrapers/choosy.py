import requests
import logging
from bs4 import BeautifulSoup
from ..helpers.categories import assign_tags

# Gets all sale items from choosy and saves them to the database under 'choosy'
def choosy(sex, store_page):
    logging.info(f'Getting {sex} choosy items from page: {store_page}')
    logging.info('Contacting choosy.com')

    # Request details
    if sex == 'female':
        url = f'https://getchoosy.com/collections/sale?page={store_page}'
    else:
        raise Exception('Invalid selection for sex, must be male or female')

    page_results = requests.get(url)

    # Could not get results from choosy
    if page_results.status_code not in [200, 201]:
        logging.info('Could not contact choosy.com')
        raise Exception('Invalid request to choosy.com')
    
    logging.info('Got choosy items')

    # Get list of items
    soup = BeautifulSoup(page_results.text, features="lxml")
    item_container = soup.find('div', { 'class': 'grid_wrapper' })
    raw_items_list = item_container.findChildren('div', { 'class': 'plp_wrappers' })

    logging.info('Parsing choosy items')
    # All items list with details, example:
    # 'blue_shoe': {
    #     'price': 12.40,
    #     'img_url': 'blahblah.img',
    #     'item_url': 'http://choosy.com/blue-shoe'
    # }
    all_items = []
    
    # Loop through raw items list and get details, then add to all_items
    for item in raw_items_list:
        # Get item details and clean up if we found the detail 
        try:
            url = f"https://getchoosy.com{item.find('a', class_='prod_link_wrapper')['href']}"
        except:
            url = 'None'
        
        try:
            img_url = f"https:{item.find('img', class_='grid_img')['src']}"
        except:
            img_url = 'None'
        
        try:
            name = item.find('p', { 'class': 'product_title' }).getText().replace('\n', '').replace('\t', '').replace('\r', '').replace('$', '').strip()
        except:
            name = 'None'

        try:
            sale_price = float(item.find('span', class_='current_price red').text.replace('\n', '').replace('\t', '').replace('\r', '').replace('$', '').strip())
        except:
            sale_price = 'None'

        try:
            normal_price = float(item.find('span', { 'class': 'prev_price' }).text.replace('\n', '').replace('\t', '').replace('\r', '').replace('$', '').strip())
        except:
            normal_price = 'None'

        # GET TAGS HERE
        tags = assign_tags(name, category_type="clothing")
        
        # Add item with details to list
        all_items.append({
            'url': url,
            'img_url': img_url,
            'brand': 'Choosy',
            'name': name,
            'sex': sex,
            'sale_price': sale_price,
            'normal_price': normal_price,
            'type': 'clothing',
            'tags': tags,
        })

    return all_items
