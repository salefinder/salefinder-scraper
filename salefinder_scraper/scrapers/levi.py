import os
import time
import logging
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from ..helpers.categories import assign_tags

# Gets all sale items from levi and saves them to the database under 'levi'
def levi(sex, page_number, all_items):
    logging.info(f'Page {page_number}: Launching controlled selenium window...')
    
    # Selenium to open window so we can get HTML, since it's javascript rendered
    if sex == 'female':
        url = f'https://www.levi.com/US/en_US/sale/womens-sale/c/levi_clothing_women_sale_us?page={page_number}'
    elif sex == 'male':
        url = f'https://www.levi.com/US/en_US/sale/mens-sale/c/levi_clothing_men_sale_us?page={page_number}'
    else:
        raise Exception('Invalid selection for sex, must be male or female')

    options = Options()
    options.headless = True
    options.add_argument('--no-sandbox')
    options.add_argument('--window-size=1420,1080')
    options.add_argument('disable-gpu')
    browser = webdriver.Chrome(os.getenv('CHROMEDRIVER_PATH'), options=options)
    browser.get(url)

    logging.info(f'Page {page_number}: window has been launched')
    
    # Give page time to load all items
    logging.info(f'Page {page_number}: Waiting 5 seconds for page to load...')
    time.sleep(5)

    logging.info(f'Page {page_number}: Locating products on page')

    # Find all items, add to total items list
    try:
        item_container = browser.find_element_by_xpath('/html/body/main/div[6]/div[3]/div[2]/div/div[3]/div[2]')
        raw_items_list = item_container.find_elements_by_class_name('product-item')
    except:
        try:
            raw_items_list = browser.find_elements_by_class_name('product-item')
        except Exception:
            raise Exception(f'Page {page_number}: Could not locate items for parsing')

    logging.info(f'Page {page_number}: found {len(raw_items_list)} items')
    logging.info(f'Page {page_number}: items located!')
    logging.info(f'Page {page_number}: parsing items')
    
    # Loop through raw items list and get details, then add to all_items
    for item in raw_items_list:
        # Get item details
        try:
            url = item.find_element_by_tag_name('a').get_attribute('href')
        except:
            url = 'None'

        try:
            img_url = item.find_element_by_tag_name('img').get_attribute('src')

            if img_url == "":
                try:
                    img_url = item.find_element_by_tag_name('img').get_attribute('data-src')
                except:
                    img_url = 'None'
        except:
            try:
                img_url = item.find_element_by_tag_name('img').get_attribute('data-src')
            except:
                img_url = 'None'
        
        try:
            name = item.find_element_by_tag_name('a').get_attribute('title')
        except:
            name = 'None'
        
        try:
            sale_price = item.find_element_by_class_name('hard-sale').text.strip('$').replace(' ', '')
        except:
            try:
                sale_price = item.find_element_by_class_name('softSale').text.replace('$').replace(' ', '')
            except:
                sale_price = 'None'
        
        try:
            normal_price = item.find_element_by_class_name('regular').text.strip('$').replace(' ', '')
        except:
            try:
                normal_price = item.find_element_by_class_name('price').text.strip('$').replace(' ', '')
            except:
                normal_price = 'None'
        
        # Assign tags
        tags = assign_tags(name, category_type="clothing")

        # Add item with details to list
        all_items.append({
            'url': url,
            'img_url': img_url,
            'name': name,
            'brand': 'Levi',
            'sex': sex,
            'sale_price': sale_price,
            'normal_price': normal_price,
            'type': 'clothing',
            'tags': tags,
        })
    
    # Quit the window
    browser.quit()

    logging.info(f'Page {page_number}: thread is done')
    return