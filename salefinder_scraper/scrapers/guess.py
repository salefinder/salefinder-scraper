import requests
import logging
from bs4 import BeautifulSoup
from ..helpers.categories import assign_tags

# Gets all sale items from guess and saves them to the database under 'guess'
def guess(sex):
    logging.info(f'Getting {sex} guess items')
    logging.info('Contacting guess.com')

    # Request details
    if sex == 'female':
        url = 'https://www.guess.com/us/en/women/apparel/sale?start=0&sz=5000'
    elif sex == 'male':
        url = 'https://www.guess.com/us/en/men/apparel/sale?start=0&sz=5000'
    else:
        raise Exception('Invalid selection for sex, must be male or female')

    page_results = requests.get(url)

    # Could not get results from guess
    if page_results.status_code not in [200, 201]:
        logging.info('Could not contact guess.com')
        raise Exception('Invalid request to guess.com')
    
    logging.info('Got guess items')

    # Get list of items
    soup = BeautifulSoup(page_results.text, features="lxml")
    item_container = soup.find('div', { 'class': 'row product-grid' })
    raw_items_list = item_container.findChildren('div', { 'class': 'product-grid__column' })

    logging.info('Parsing guess items')
    # All items list with details, example:
    # 'blue_shoe': {
    #     'price': 12.40,
    #     'img_url': 'blahblah.img',
    #     'item_url': 'http://guess.com/blue-shoe'
    # }
    all_items = []
    
    # Loop through raw items list and get details, then add to all_items
    for item in raw_items_list:
        # Get item details and clean up if we found the detail 
        try:
            url = item.find('a')['href']
        except:
            url = 'None'
        
        try:
            img_url = item.findAll('img', class_='tile-image')[0]['data-src']
        except:
            img_url = 'None'

        brand = 'Guess'
        
        try:
            name = item.find('a', { 'class': 'link product-tile__link' }).getText()
        except:
            name = 'None'

        try:
            sale_price = item.find('span', class_='price__red-color').text.split('Sale')[1].replace('\n', '').replace('\t', '').replace('\r', '').replace('$', '').strip()
        except:
            try:
                sale_price = item.find('span', { 'class': 'starting' }).replace('$', '').strip()
            except:
                sale_price = 'None'

        try:
            normal_price = item.find('span', { 'class': 'price__strike-through' }).text.replace('\n', '').replace('\t', '').replace('\r', '').replace('$', '').strip()
        except:
            try:
                normal_price = item.find('span', { 'class': 'ending' }).text.replace('$', '').strip()
            except:
                normal_price = 'None'

        # Assign tags
        tags = assign_tags(name, category_type="clothing")

        # Add item with details to list
        all_items.append({
            'url': url,
            'img_url': img_url,
            'brand': brand,
            'name': name,
            'sex': sex,
            'sale_price': sale_price,
            'normal_price': normal_price,
            'type': 'clothing',
            'tags': tags,
        })

    return all_items
