import os # Grabbing env vars
import logging

from pymongo import MongoClient # Connect to database
from pymongo.errors import BulkWriteError

# Connect to database
def connect_db():
    # Connect to database
    mongo_client = MongoClient(os.getenv('MONGO_CONNECTION'))
    db = mongo_client[os.getenv('MONGO_DB')]
    return [mongo_client, db]

# Get document(s) from collection
def get_documents(collection_name, db_connection):
    logging.info(f'Getting document(s) from {collection_name}')
    try:       
        # Get collection and send back contents
        return list(db_connection[collection_name].find({}))
    except Exception as e:
        logging.error('Failed to get documents from collection')
        raise e
    
    return all_items

# Insert document(s) into collection
def insert_documents(collection_name, documents, db_connection):
    logging.info('Inserting document(s) to database')
    try:
        # Insert documents to collection, returns object of _ids
        return db_connection[collection_name].insert_many(documents)
    except BulkWriteError as e:
        logging.error('Failed to save documents to database')
        logging.error(e.details)
    except Exception as e:
        logging.error('Failed to save documents to database')
        raise e

# Delete collection from database
def delete_collection(collection_name, db_connection):
    logging.info(f'Deleting collection {collection_name} from database')
    try:
        return db_connection.drop_collection(collection_name)
    except Exception as e:
        logging.error('Failed to delete collection from database')
        raise e