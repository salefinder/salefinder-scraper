import requests
import random
from bs4 import BeautifulSoup

def get_random_user_agent():
    # Grab data
    url = 'https://developers.whatismybrowser.com/useragents/explore/software_name/chrome/'
    page_results = requests.get(url)

    # Parse
    soup = BeautifulSoup(page_results.text, features="lxml")
    table = soup.find('table')
    table_items = table.findAll('td', class_='useragent')

    # Clean
    user_agents = []
    for item in table_items:
        user_agents.append(item.text)

    return random.choice(user_agents)
