import os
import time
import logging
from ..helpers.mongohelpers import insert_documents, delete_collection
from threading import Thread

# Parallel launcher for stores that don't limit expand
def parallel_launcher(store_name, sex, store_function, page_amount):
    logging.info('')
    logging.info('')
    logging.info('Starting parallel selenium launcher...')
    logging.info(f'Getting {store_name} {sex} items...')

    # All items list with details, example:
    # 'blue_shoe': {
    #     'price': 12.40,
    #     'img_url': 'blahblah.img',
    #     'item_url': 'http://Nordstrom.com/blue-shoe'
    # }
    all_items = []

    # Page
    page_number = 1
    
    # Threading
    max_threads = None
    
    if page_amount < int(os.getenv('MAX_THREADS')):
        max_threads = page_amount
    else:
        max_threads = int(os.getenv('MAX_THREADS'))
        
    remaining_threads = []
    running_threads = []

    logging.info('Creating threads for all pages...')

    # Create thread for each page, add to threads list
    while page_number <= page_amount:
        new_thread = Thread(
            target=store_function,
            args=(sex, page_number, all_items),
            name=(page_number)
        )
        remaining_threads.append(new_thread)
        page_number += 1

    logging.info(f'Created { len(remaining_threads) } threads to run')
    logging.info('Starting threads...')

    # Keep running until we out of remaining threads   
    while len(remaining_threads) > 0:
        # Rev up on start new threads to max
        if len(remaining_threads) == page_amount:
            logging.info('Launching quickbois...')

            for page_thread in remaining_threads[:max_threads - 1]:
                # Grab first thread, start, and add to running threads list
                page_thread.start()
                running_threads.append(page_thread)

                # Remove thread from remaining, since its running
                remaining_threads.remove(page_thread)
            
            logging.info('')
            logging.info(f'REMAINING PAGES: { len(remaining_threads) }/{ page_amount }')
            logging.info(f'RUNNING THREADS: { len(running_threads) }/{ max_threads }')

            
        # Cleanup any dead threads
        for page_thread in running_threads:
            if not page_thread.is_alive():
                running_threads.remove(page_thread)
                logging.info('')
                logging.info(f'REMAINING PAGES: { len(remaining_threads) }/{ page_amount }')
                logging.info(f'RUNNING THREADS: { len(running_threads) }/{ max_threads }')

        # Check for space to start new thread
        if len(running_threads) <= max_threads:
            logging.info('')
            logging.info('Starting thread...')
            
            # Grab first thread, start, and add to running threads list
            page_thread = remaining_threads[0]
            page_thread.start()
            running_threads.append(page_thread)

            # Remove thread from remaining, since its running
            remaining_threads.remove(page_thread)

            logging.info('')
            logging.info(f'REMAINING PAGES: { len(remaining_threads) }/{ page_amount }')
            logging.info(f'RUNNING THREADS: { len(running_threads) }/{ max_threads }')

        # Cooldown until next check
        time.sleep(.2)
        logging.debug('All threads are in use, checking again')

    if len(running_threads) != 0:
        print('')
        logging.info('Letting remaining page bois cooldown...')

    # Cleanup remaining threads
    while len(running_threads) > 0:
        for thread in running_threads:
            if not thread.is_alive():
                running_threads.remove(thread)

        logging.info(f'RUNNING THREADS: { len(running_threads) }/{ max_threads }')
        time.sleep(1)

    logging.info('All threads done!')
    logging.info('')
    logging.info('')
    logging.info(f'Parallel selenium launcher for {store_name} is done!')
    logging.info(f'Total items: { len(all_items) }')
    logging.info('')
    logging.info('')
    return all_items
