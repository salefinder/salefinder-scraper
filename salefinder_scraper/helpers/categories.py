import json
import pathlib # path
import logging
import re

# Assigns tags based on results found in name
def assign_tags(strings_to_check, category_type = None):
    if not strings_to_check:
        raise KeyError('Missing strings_to_check')
    elif not category_type:
        raise KeyError("Missing category_type keyword parameter")

    logging.debug(f'Assigning tags: {strings_to_check}')

    # Convert name to list if not passed in
    if type(strings_to_check) is not list:
        strings_to_check = [strings_to_check]

    # Load in json categories
    categories = None
    current_path = pathlib.Path().absolute()
    with open(f'{current_path}/salefinder_scraper/helpers/categories.json') as file:
        json_doc = json.load(file)
        categories = json_doc[category_type]
        global_excluded_words = json_doc['excluded_words']

    # Assign tags based on name
    tags = []
    for single_string in strings_to_check:
        for sub_type in categories:
            # Groups contain multiple items of same type; tops, bottoms, etc.
            # Names of items within a group; ex. shirt, tshirt, top
            keywords = categories[sub_type]['words']
            all_excluded_keywords = global_excluded_words + categories[sub_type]['excluded']
            cleaned_string = None
            did_clean = False

            # Remove any words that may confused tagger
            for excluded_word in all_excluded_keywords:
                if excluded_word in single_string.lower():
                    excluded_word = re.compile(re.escape(excluded_word), re.IGNORECASE)
                    cleaned_string = excluded_word.sub('', single_string)
                    did_clean = True

            # Add tags if found
            for word in keywords:
                if did_clean:
                    if word in cleaned_string.lower() and word not in tags:
                        tags.append(word)
                else:
                    if word in single_string.lower() and word not in tags:
                        tags.append(word)
    return tags